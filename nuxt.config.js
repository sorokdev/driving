export default {
    // Disable server-side rendering: https://go.nuxtjs.dev/ssr-mode
    ssr: false,

    // Global page headers: https://go.nuxtjs.dev/config-head
    head: {
        title: 'driveduc',
        htmlAttrs: {
            lang: 'en'
        },
        meta: [
            { charset: 'utf-8' },
            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            { hid: 'description', name: 'description', content: '' },
            { name: 'format-detection', content: 'telephone=no' }
        ],
        link: [
            { rel: 'icon', type: 'image/png', href: '/myfavicon.png' }
        ],
        script: [{
                body: true,
                src: 'https://unpkg.com/browse/bootstrap@5.1.3/dist/js/bootstrap.min.js',
            },
            {
                body: true,
                src: "https://unpkg.com/ionicons@5.0.0/dist/ionicons.js",
            },
            {
                body: true,
                src: "https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.min.js",
            },
            {
                body: true,
                src: "https://cdn.jsdelivr.net/npm/admin-lte@3.2/dist/js/adminlte.min.js",
            },

        ],
    },

    // Global CSS: https://go.nuxtjs.dev/config-css
    css: [
        '~/node_modules/bootstrap/dist/css/bootstrap.css',
        "~/node_modules/admin-lte/dist/css/adminlte.min.css",
        "~/node_modules/uikit/dist/css/uikit.css",
        "~/assets/main.css",


    ],


    // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
    plugins: [
        { src: "~/plugins/uikit", ssr: false },
        '~/plugins/vue-tour',
        '~/plugins/vue-introjs'
    ],

    buildModules: [],


    // Auto import components: https://go.nuxtjs.dev/config-components
    components: true,

    // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules

    // Modules: https://go.nuxtjs.dev/config-modules
    modules: [
        // https://go.nuxtjs.dev/axios
        '@nuxtjs/auth',
        '@nuxtjs/axios',
        'vue-sweetalert2/nuxt',
    ],
    auth: {
        redirect: {
            login: '/auth',
            logout: '/auth',
            callback: '/auth',
            home: '/driver'
        },
        strategies: {

            user: {
                _scheme: 'local',
                endpoints: {

                    login: {
                        url: '/auth/login',
                        method: 'post',
                        propertyName: 'data.token'
                    },
                    // admin: { url: 'admin/auth/login', method: 'post', propertyName: 'data.data.token' },
                    user: {
                        url: '/driver/profil',
                        method: 'get',
                        propertyName: 'data'
                    },
                    logout: false
                }

            },

            admin: {
                _scheme: 'local',
                endpoints: {

                    login: {
                        url: '/admin/auth/login',
                        method: 'post',
                        propertyName: 'data.token'
                    },
                    // admin: { url: 'admin/auth/login', method: 'post', propertyName: 'data.data.token' },
                    user: {
                        url: '/admin/profil',
                        method: 'get',
                        propertyName: ''
                    },
                    logout: false
                }

            },

        },
        watchLoggedIn: true
    },

    sweetalert: {
        confirmButtonColor: '#41b882',
        cancelButtonColor: '#ff7674'
    },

    // Axios module configuration: https://go.nuxtjs.dev/config-axios
    axios: {
        // Workaround to avoid enforcing hard-coded localhost:3000: https://github.com/nuxt-community/axios-module/issues/308
        baseURL: 'http://127.0.0.1:3333',
    },
    watchLoggedIn: true,

    sweetalert: {
        confirmButtonColor: '#41b882',
        cancelButtonColor: '#ff7674'
    },

    // Axios module configuration: https://go.nuxtjs.dev/config-axios
    axios: {
        // Workaround to avoid enforcing hard-coded localhost:3000: https://github.com/nuxt-community/axios-module/issues/308
        baseURL: 'http://192.168.252.193:3333',
    },

    // Build Configuration: https://go.nuxtjs.dev/config-build
    build: {

    },

    server: {
        host: '0'
    },


}