import Swal from 'sweetalert2';
export default function ({ store, redirect }) {
  // If the user is not authenticated
  console.log(store.state.auth.user)
   if (!store.state.auth.user) {
    return redirect('/auth/admin')
  }else{
    if(!store.state.auth.user.admin){
      Swal.fire("", "Accès non autorisé", "error");
      return redirect('/auth/admin')
    }
  }
}
